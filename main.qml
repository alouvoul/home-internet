import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.4

Window {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    title:qsTr("Internet home")
    minimumHeight:480
    minimumWidth:640
    maximumHeight: minimumHeight
    maximumWidth: minimumWidth
    Rectangle
    {
        anchors.fill: parent
        anchors.margins: 1
        color:"pink"

        Component{
            id: homeQml
            Home{}
        }
        StackView{
            anchors.fill: parent
            initialItem: homeQml
            id: stack
        }


    }
    function backButton(){
        stack.pop();
    }
}

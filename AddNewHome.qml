import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.4

Rectangle {
    id:addNewHome
    anchors.fill: parent
    Topbar{

        id : bar
        Text{
            text: qsTr("Create a new home")
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter:  parent.horizontalCenter
            font.pixelSize: parent.height*0.37
            color: "#ffffff"
            wrapMode: Text.Wrap
        }
        BackButton{
            id:bb
            onClicked: {
                backButton();
            }
        }
    }
    Image{
        id:backgroundImage
        clip: false
        anchors.top: bar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        source: "images/mainphoto.jpg"
        visible: true
        Rectangle{
            id:textAndtextfield
            anchors.top:backgroundImage.top
            anchors.topMargin: backgroundImage.height*0.04
            anchors.horizontalCenter: backgroundImage.horizontalCenter
            width: name.width+nameTextfield.width
            Text{
                anchors.top:textAndtextfield.top
                id:name
                text: "Name"
                font.pixelSize: 16
            }
            TextField{
                id:nameTextfield
                anchors.top:textAndtextfield.top
                anchors.left: name.right
                style: TextFieldStyle {
                    textColor: "black"
                    selectionColor: "#303030"
                    background: Rectangle {
                        radius: 5
                        color: "#ffffff"
                        border.color: "#9e9e9e"
                        border.width: 1
                    }
                }
            }
        }


        Button{
            id: buttonMessage
            height: 50
            width: 100
            text: "Save"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom

            y: 0

            anchors.margins: 10
            onClicked:{
                anchors.fill; parent
                //house.initialize();
                mediator.insertHouse(qsTr(nameTextfield.text));

                view.model=mediator.myModel
                stack.pop();
            }
        }
    }
}

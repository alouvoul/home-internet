import QtQuick 2.0
import QtQuick.Controls 1.4


Rectangle{
    id:roomOptionView
    anchors.fill: parent
    property bool onOff: true
    Topbar{

        id : bar
        Text{
            text: qsTr("Control your room")
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter:  parent.horizontalCenter
            font.pixelSize: parent.height*0.37
            color: "#ffffff"
            wrapMode: Text.Wrap
        }
        BackButton{
            id:bb
            onClicked: {
                backButton();
            }
        }
    }
    Image{
        id:backgroundImage
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        clip: false
        anchors.top: bar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        source: "images/mainphoto.jpg"
        visible: true


        GroupBox {
            id: groupBox1
            anchors.horizontalCenter: backgroundImage.horizontalCenter
            anchors.verticalCenter: backgroundImage.verticalCenter
            width: parent.width*0.9
            height: parent.height*0.9
            checked: false
            title: qsTr("Change Room's Vallues")

            Text {
                id: lightText
                x: groupBox1.width*0.1
                y: groupBox1.height*0.2
                width: 62
                height: 25
                text: qsTr("Light")
                rotation: 0
                scale: 1.3
                font.pixelSize: 12
            }
            CustonButton{
                width: 80
                height: 80
                x: groupBox1.width*0.2
                y: groupBox1.height*0.1
                id:light
                imageA:qsTr("images/lightoff.png")
                imageB:qsTr("images/lighton.png")
                property bool lightB : mediator.getRoomLight(view.currentIndex,houseListView.currentIndex)
                asd:if(lightB){"images/lighton.png"}else{"images/lightoff.png"}



                onClicked:
                {
                    if(lightB){
                        lightB=false
                    }else{
                        lightB=true
                    }

                }
            }

            Text {
                id: airText
                x: groupBox1.width*0.1
                y: groupBox1.height*0.35
                width: 62
                height: 25
                text: qsTr("AirCondition")
                rotation: 0
                scale: 1.3
                font.pixelSize: 12
            }
            SpinBox
            {
                id:spinbox
                x: groupBox1.width*0.3
                y: groupBox1.height*0.3
                height: 40
                width: 80
                stepSize: 0.1
                decimals: 1
                value: mediator.getRoomAirCondition(view.currentIndex,houseListView.currentIndex)
            }

            Button {
                id: done
                x: groupBox1.width*0.4
                y: groupBox1.height*0.6
                text: qsTr("Save")
                scale: 1.3
                onClicked: {
                    mediator.setValues(view.currentIndex,houseListView.currentIndex,spinbox.value,light.lightB);
                    backButton();
                }
            }
        }

    }
}

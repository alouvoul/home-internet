#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QStringList>
#include <qqmlcontext.h>
#include "myHouse.h"
#include "HouseModel.h"
#include "mediator.h"

#include <iostream>
#include <fstream>
#include <qquickview.h>
using namespace std;

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    Mediator *msg = new Mediator();

    engine.rootContext()->setContextProperty("mediator",msg);
    //msg->myModel()->getHouses();


    QStringList dataList;
    dataList.append("Living room");
    dataList.append("Bedroom");
    dataList.append("Sitting room");
    dataList.append("Child's room");
    dataList.append("General");

    engine.rootContext()->setContextProperty("roommodel", dataList);

    //engine.rootContext()->setContextProperty("house",msg->myModel()->getHouses());
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

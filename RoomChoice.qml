import QtQuick 2.0
import QtQuick.Controls 1.4


Rectangle{
    id:roomChoice
    anchors.fill: parent
    Topbar{

        id : bar
        Text{
            text: qsTr("Select a room " )
            anchors.verticalCenterOffset: 3
            anchors.horizontalCenterOffset: -191
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter:  parent.horizontalCenter
            font.pixelSize: parent.height*0.37
            color: "#ffffff"
            wrapMode: Text.Wrap
        }
        BackButton{
            id:bb
            onClicked: {
                backButton();
            }
        }

        Text {
            id: text1
            x: 427
            y: 13
            width: 93
            height: 14
            color: "#ffffff"
            text: "General Options"
            font.pixelSize: parent.height*0.37
        }
    }
    Image{
        id:backgroundImage
        clip: false
        anchors.top: bar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        source: "images/mainphoto.jpg"
        visible: true
        Rectangle
        {
            y: 0

            border.color: "black";
            color: "transparent"
            anchors.horizontalCenterOffset: -160

            height: parent.height
            width: parent.width/2
            anchors.horizontalCenter: parent.horizontalCenter

            ListView
            {
                id:houseListView

                anchors.fill: parent
                model:roommodel
                delegate: listDelegate

                highlight: Rectangle
                {
                width: parent.width
                color: "lightgray"
            }
        }
    }

        GroupBox {
            id: groupBox1
            width: (backgroundImage.width/2)*0.9
            height: backgroundImage.height*0.9
            y:(backgroundImage.height/2)*0.1/2
            x: backgroundImage.width/2+(backgroundImage.width/2)*0.1/2
            title: qsTr("Control your House")

            Text {
                id: lightText
                x: groupBox1.width*0.1
                y: groupBox1.height*0.2
                width: 62
                height: 25
                text: qsTr("Heat")
                rotation: 0
                scale: 1.3
                font.pixelSize: 12
            }
            CustonButton{

                width: 80
                height: 80
                x: groupBox1.width*0.2
                y: groupBox1.height*0.1
                id:heat
                imageA:qsTr("images/heatOFF.png")
                imageB:qsTr("images/heatON.png")
                property bool heatB : mediator.getHouseHeater(view.currentIndex)
                asd:if(heatB){"images/heatON.png"}else{"images/heatOFF.png"}
                onClicked:
                {
                    if(heatB){
                        heatB=false
                    }else{
                        heatB=true
                    }

                }
            }
            Slider {
                id: radiatorSlider
                x: groupBox1.width*0.35
                y: groupBox1.height*0.35
                width: 100
                height: 22
                stepSize: 0.5
                value: mediator.getHouseRadiator(view.currentIndex)
            }

            Text {
                id: text2
                x: groupBox1.width*0.1
                y: groupBox1.height*0.35
                width: 78
                height: 14
                text: qsTr("Radiator")
                font.pixelSize: 16
            }

            Button {
                id: generalOptionsButton
                x: 108
                y: 202
                text: qsTr("Save")
                scale: 1.3
                onClicked: mediator.setGeneralOptions(view.currentIndex,radiatorSlider.value,heat.heatB)
            }
        }
    }
    /*ListModel
    {
        id:houseListModel
        ListElement{
            name:"Living room"

        }
        ListElement{
            name:"Bedroom"

        }
        ListElement{
            name:"Sitting room"

        }
        ListElement{
            name:"Child's room"

        }
        ListElement{
            name:"General"

        }
    }*/
    /*
        Component to declare delegate of the ListView for the rooms
    */
    Component
    {
        id: listDelegate
        Item
        {
            width:parent.width
            height: 30
            Text
            {
                id:roomName
                font.pixelSize: 16
                text:modelData
            }

            MouseArea
            {
                anchors.fill: parent
                onClicked: houseListView.currentIndex=index
                onDoubleClicked: {
                    houseListView.currentIndex=index
                    //getValues(houseListView.currentIndex);
                    stack.push(roomView)
                }
            }
        }


    }
    Component
    {
        id:roomView
        RoomOptions{}
    }
    /*function getValues(){
        mediator.getRoomLight(view.currentIndex)
        mediator.getRoomAirCondition(houseListView.currentIndex)
    }*/
}

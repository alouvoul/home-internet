#ifndef MEDIATOR_H
#define MEDIATOR_H

#include <QObject>
#include <QDesktopServices>
#include"HouseModel.h"

class Mediator : public QObject
{
    HouseModel *houses;

    Q_OBJECT
    Q_PROPERTY(HouseModel* myModel READ myModel WRITE setColorModel NOTIFY colorModelChanged)
    //Q_PROPERTY(HouseModel* myModel READ myModel WRITE houses->getHouses() NOTIFY colorModelChanged)

public:
    //QString s =":/houses.txt";
    QString s = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation)+"/houses.txt" ;

    explicit Mediator(QObject *parent = 0);
    void setColorModel(HouseModel* m)
    {
        houses = m;
        emit colorModelChanged();
    }
    HouseModel* myModel()
    {
        return houses;
    }
signals:
    void colorModelChanged();
public slots:
    void insertHouse(QString colorName);
    /*QString getHouseName(int index){
        QString *name;
        houses[0].getColor(index,*name);
        return *name;
    }*/

    void deleteHouse(int row);
    bool getRoomLight(int house, int room);
    float getRoomAirCondition(int house, int room);
    void setValues(int house, int room,float airConditionValue, bool lighterValue);

    bool getHouseHeater(int house);
    float getHouseRadiator(int house);
    void setGeneralOptions(int house,float radiatorValue, bool heaterValue);
};

#endif // MEDIATOR_H

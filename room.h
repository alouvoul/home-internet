#ifndef ROOM_H
#define ROOM_H

#include <QString>



class Room
{
public:
    Room();
    void setRoomName(QString name);
    QString getRoomName();

    void setAirCondition(float t);
    void setLighter(bool t);

    float getAirCondition();
    bool getLighter();

private:
    QString roomName;
    float airCondition = 0.0;
    bool lighter = false;

};

#endif // ROOM_H

#ifndef COLORMODEL_H
#define COLORMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include "myHouse.h"

class HouseModel : public QAbstractListModel
{
    Q_OBJECT
    public:
        enum ColorRoles {
            NameRole = Qt::UserRole + 1,
            ColorRole
        };


        HouseModel(QObject *parent = 0);

        void addColor(const myHouse &color);
        void getColor(int row, QString &name);
        QStringList   getHouses();//taiobusda
        int rowCount(const QModelIndex & parent = QModelIndex()) const;


        QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

        void deleteColor(int row);

        bool getLight(int index,int room);
        float getAirCondition(int index,int room);
        void setValues(int index,int room,float airConditionValue, bool lighterValue);
        //General options control
        bool getHeater(int index);
        float getRadiator(int index);
        void setGeneralOptions(int index,float radiatorValue, bool heaterValue);
    protected:
        QHash<int, QByteArray> roleNames() const;
    private:
        QList<myHouse> houses;
};

#endif // COLORMODEL_H

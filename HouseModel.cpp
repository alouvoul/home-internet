#include "HouseModel.h"

HouseModel::HouseModel(QObject *parent): QAbstractListModel(parent)
{
}

void HouseModel::addColor(const myHouse &color)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    houses << color;
    endInsertRows();
}

void HouseModel::deleteColor(int row)
{
    beginRemoveRows(QModelIndex(),row,row);
    houses.removeAt(row);
    endRemoveRows();
}

void HouseModel::getColor(int row, QString &mname)
{
    mname=houses[row].name();
}


int HouseModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return houses.count();
}

QStringList HouseModel::getHouses(){
    QStringList temp;

    for(int i = 0; i < houses.length();i++)
        temp.append(houses.at(i).name());

    return temp;
}
//*************************************************************************
QVariant HouseModel::data(const QModelIndex & index, int role) const {
    if (index.row() < 0 || index.row() >= houses.count())
        return QVariant();

    const myHouse &color = houses[index.row()];
    if (role == NameRole)
        return color.name();
    else if (role == ColorRole)
        return color.name();//color.color();
    return QVariant();
}


QHash<int, QByteArray> HouseModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    return roles;
}
/*
 *
 */
bool HouseModel::getLight(int index,int room){
    return houses[index].getLight(room);
}
float HouseModel::getAirCondition(int index,int room){
    return houses[index].getAirCondition(room);
}

void HouseModel::setValues(int index, int room,float airConditionValue, bool lighterValue){
    houses[index].setAirCondition(room,airConditionValue);
    houses[index].setLight(room,lighterValue);
}


bool HouseModel::getHeater(int index){
    return houses[index].getHeater();
}

float HouseModel::getRadiator(int index){
    return houses[index].getRadiator();
}

void HouseModel::setGeneralOptions(int house,float radiatorValue, bool heaterValue){
    houses[house].setRadiator(radiatorValue);
    houses[house].setHeater(heaterValue);
}



















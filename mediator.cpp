#include "mediator.h"
#include <QFile>
#include <QStandardPaths>
#include <QTextStream>

Mediator::Mediator(QObject *parent) : QObject(parent)
{
    houses = new HouseModel();

    qDebug(s.toLatin1());
    QFile qf(s);
    qf.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream in(&qf);
    while (!in.atEnd())
    {
        QString cname = in.readLine();
        houses->addColor(myHouse(cname));
    }
    qf.close();

}

void Mediator::insertHouse(QString colorName)
{
    houses->addColor(myHouse(colorName));

    QFile qf(s);
    QTextStream out(&qf);

    if(!qf.open(QIODevice::Append | QIODevice::Text )) {
        qDebug("Den anoikse to arxeio");
    }

    out<<colorName<<endl;
    qf.close();
}

void Mediator::deleteHouse(int row)
{
    houses->deleteColor(row);

    QFile qf(s);
    QTextStream out(&qf);

    qf.open(QIODevice::WriteOnly | QIODevice::Text);

    for (int i=0;i<houses->rowCount();i++)
    {
        QString n,c;
        houses->getColor(i,n);
        out<<n<<endl;
        //out<<c<<endl;
    }
    qf.close();
}

bool Mediator::getRoomLight(int house, int room){
    return houses->getLight(house,room);
}

float Mediator::getRoomAirCondition(int house, int room){
    return houses->getAirCondition(house,room);
}

void Mediator::setValues(int house, int room,float airConditionValue, bool lighterValue){
    houses->setValues(house,room,airConditionValue,lighterValue);
}

//*********Control for general options*********/
bool Mediator::getHouseHeater(int house){
    return houses->getHeater(house);
}

float Mediator::getHouseRadiator(int house){
    return houses->getRadiator(house);
}

void Mediator::setGeneralOptions(int house,float radiatorValue, bool heaterValue){
    houses->setGeneralOptions(house,radiatorValue, heaterValue);
}









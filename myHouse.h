#ifndef MYCOLOR_H
#define MYCOLOR_H
#include <QString>

#include "room.h"

class myHouse
{
public:
    myHouse(const QString &name);

    QString name() const;
    void setName(const QString &name);
    void setRooms(int index, float air, bool lighter);

    bool getLight(int index);
    float getAirCondition(int index);
    void setAirCondition(int index,float value);
    void setLight(int index,bool value);
    bool getHeater();
    float getRadiator();
    void setHeater(bool value);
    void setRadiator(float value);

private:
    QString m_name;
    Room rooms[5];
    bool heater;
    float radiator;
};

#endif // MYCOLOR_H

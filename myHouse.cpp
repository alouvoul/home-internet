#include "myHouse.h"

#include <QQuickView>
#include <QQmlContext>

myHouse::myHouse(const QString &name) : m_name(name)
{
    //rooms = new Rooms();
    heater = false;
    rooms[0].setRoomName("Living room");
    rooms[1].setRoomName("Bedroom");
    rooms[2].setRoomName("Sitting room");
    rooms[3].setRoomName("Child's room");
    rooms[4].setRoomName("General");

}

QString myHouse::name() const
{
    return m_name;
}

void myHouse::setName(const QString &name)
{
    if (name != m_name) {
        m_name = name;
    }
}


void myHouse:: setRooms(int index, float air, bool lighter){

    rooms[index].setAirCondition(air);
    rooms[index].setLighter(lighter);
}

bool myHouse::getLight(int index){
    return rooms[index].getLighter();
}

float myHouse:: getAirCondition(int index){
    return rooms[index].getAirCondition();
}

void myHouse:: setLight(int index,bool value){
    rooms[index].setLighter(value);
}


void myHouse:: setAirCondition(int index,float value){
    rooms[index].setAirCondition(value);
}

//*****Methods to control general options*******/
bool myHouse::getHeater(){
    return heater;
}

float myHouse::getRadiator(){
    return radiator;
}

void myHouse::setRadiator(float value){
    radiator = value;
}

void myHouse::setHeater(bool value){
    heater = value;
}

/*QString myHouse::color() const
{
    return m_color;
}
*/

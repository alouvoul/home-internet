import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick 2.0
Rectangle{


    Topbar{
        id : bar
        Text{
            text: qsTr("Select a home")
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter:  parent.horizontalCenter
            font.pixelSize: parent.height*0.37
            color: "#ffffff"
            wrapMode: Text.Wrap
        }
    }
    Image{
        id:backgroundImage
        clip: false
        anchors.top: bar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        source: "images/mainphoto.jpg"
        visible: true
        PathView {
            id:view
                anchors.fill: parent

                delegate: flipCardDelegate
                model: mediator.myModel

                path: Path {
                    startX: backgroundImage.width/2
                    startY: 0
                    PathAttribute { name: "itemZ"; value: 0 }
                    PathAttribute { name: "itemAngle"; value: -90.0; }
                    PathAttribute { name: "itemScale"; value: 0.5; }
                    PathLine { x: backgroundImage.width/2; y: backgroundImage.height*0.4; }
                    PathPercent { value: 0.48; }
                    PathLine { x: backgroundImage.width/2; y: backgroundImage.height*0.5; }
                    PathAttribute { name: "itemAngle"; value: 0.0; }
                    PathAttribute { name: "itemScale"; value: 1.0; }
                    PathAttribute { name: "itemZ"; value: 100 }
                    PathLine { x: backgroundImage.width/2; y: backgroundImage.height*0.6; }
                    PathPercent { value: 0.52; }
                    PathLine { x: backgroundImage.width/2; y: backgroundImage.height; }
                    PathAttribute { name: "itemAngle"; value: 90.0; }
                    PathAttribute { name: "itemScale"; value: 0.5; }
                    PathAttribute { name: "itemZ"; value: 0 }
                }
                pathItemCount: 16
                preferredHighlightBegin: 0.5
                preferredHighlightEnd: 0.5

            }
        Image{
            id:arrow
            clip: false
            source: "images/arrow.png"
            visible: true
            height: 50
            width:50
            x:backgroundImage.width/2-100
            y:bar.height+backgroundImage.height/2-65
        }

        Button {
            id: deleteButton
            height: 40
            width: 100
            x: 540
            y: 402
            text: qsTr("Delete")
            opacity: 0.7
            onClicked: mediator.deleteHouse(view.currentIndex)
        }
        Button
        {
            height: 40
            width: 100
            text: "Add"
            opacity: 0.7
            anchors.bottom: parent.bottom

            onClicked:{
                stack.push(addNewHouseQml);
            }
        }
    }
    Component {
        id: flipCardDelegate
        HomeOptionsDelegate {
            id: wrapper
            antialiasing: true
            visible: PathView.onPath
            scale: PathView.itemScale
            z: PathView.itemZ
            property variant rotX: PathView.itemAngle
            transform: Rotation {
                axis { x: 1; y: 0; z: 0 }
                angle: wrapper.rotX;
                origin { x: 32; y: 32; }
            }
        }
    }
    Component{
        id:addRoomsQml
        RoomChoice{}
    }
    Component
    {
        id:addNewHouseQml
        AddNewHome{}
    }

}


#include "room.h"

Room::Room()
{
    airCondition = 2.2;
    lighter = false;
}

void Room::setRoomName(QString name){
    roomName = name;
}

void Room::setAirCondition(float t)
{
    airCondition = t;
}

void Room::setLighter(bool t)
{
    lighter = t;
}

float Room::getAirCondition()
{
    return airCondition;
}

bool Room::getLighter()
{
    return lighter;
}

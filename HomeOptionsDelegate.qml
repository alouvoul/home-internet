import QtQuick 2.0

Item{
       //anchors.centerIn: parent
       id:deleText;
       height: 68
       width: 68
       Column{
           id: opt
           //anchors.fill: parent
           height: 68
           width: 68
           Image {
               id: homeImage
               source: "images/home.png"
               //anchors.top: parent.top
               anchors.horizontalCenter: parent.horizontalCenter
               width: 64;
               height: 64
           }

           Text {
               x: 5
               id: name1
               text: name     //modelData set text as given from .cpp file
               anchors.horizontalCenter: parent.horizontalCenter
           }

       }
       //When clicked on a delegate change view to a specified house
       MouseArea{
           anchors.fill: parent
           onClicked: {
               console.log("Pressed!");
               console.log(name.text);

               stack.push(addRoomsQml)
           }
       }
    }
